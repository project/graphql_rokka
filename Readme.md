CONTENTS OF THIS FILE
---------------------

* Introduction
* Requirements
* Installation
* Configuration
* Maintainers


INTRODUCTION
------------

The GraphQL Rokka module exposed the [rokka.io Image](https://rokka.io) CDN fields for media and image fields in GraphQL queries.

* For a full description of the module visit:
  https://www.drupal.org/project/graphql_rokka

* To submit bug reports and feature suggestions, or to track changes visit:
  https://www.drupal.org/project/issues/graphql_rokka


REQUIREMENTS
------------

This module requires the [GraphQL Module 3](https://www.drupal.org/project/graphql) and the [rokka.io module](https://www.drupal.org/project/rokka).


INSTALLATION
------------

* Install the GraphQL Rokka module as you would normally install a
  contributed Drupal module. Visit https://www.drupal.org/node/1897420 for
  further information.


CONFIGURATION
-------------

There is no real configuration necessary. Navigate to Administration >
Extend and enable the module.

Any field with a StreamWrapper using rokka:// will expose additional fields via GraphQL API queries.


MAINTAINERS
-----------

* ayalon - https://www.drupal.org/u/ayalon

Supporting organization:

* Liip - https://www.drupal.org/liip
