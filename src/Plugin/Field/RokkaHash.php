<?php

namespace Drupal\graphql_rokka\Plugin\Field;

use Drupal\Core\Field\FieldItemList;
use Drupal\Core\TypedData\ComputedItemListTrait;
use Drupal\file\FileInterface;

/**
 * Computed Rokka hash.
 */
class RokkaHash extends FieldItemList {

  use ComputedItemListTrait;

  /**
   * {@inheritdoc}
   */
  protected function computeValue() {
    $entity = $this->getEntity();
    if ($entity instanceof FileInterface) {

      /** @var \Drupal\rokka\RokkaServiceInterface $rokkaService */
      $rokkaService = \Drupal::service('rokka.service');
      $metas = $rokkaService->loadRokkaMetadataByUri($entity->getFileUri());

      if (!empty($metas)) {
        /** @var \Drupal\rokka\Entity\RokkaMetadata $meta */
        $meta = reset($metas);
        $rokka_metadata = [
          'hash' => $meta->getHash(),
          'format' => $meta->getFormat(),
          'height' => $meta->getHeight(),
          'width' => $meta->getWidth(),
          'filename' => basename($meta->getUri()),
        ];
        $this->setValue($rokka_metadata, FALSE);

      }
    }
  }

}
