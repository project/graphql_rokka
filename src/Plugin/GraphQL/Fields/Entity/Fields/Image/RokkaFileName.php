<?php

namespace Drupal\graphql_rokka\Plugin\GraphQL\Fields\Entity\Fields\Image;

use Drupal\graphql\GraphQL\Execution\ResolveContext;
use Drupal\graphql\Plugin\GraphQL\Fields\FieldPluginBase;
use Drupal\image\Plugin\Field\FieldType\ImageItem;
use Drupal\rokka\Entity\RokkaMetadata;
use GraphQL\Type\Definition\ResolveInfo;

/**
 * Retrieve the filename.
 *
 * @GraphQLField(
 *   id = "rokka_filename",
 *   secure = true,
 *   name = "rokkaFilename",
 *   type = "String",
 *   provider = "image",
 *   field_types = {"image"},
 *   deriver = "Drupal\graphql_core\Plugin\Deriver\Fields\EntityFieldPropertyDeriver"
 * )
 */
class RokkaFileName extends FieldPluginBase {

  /**
   * {@inheritdoc}
   */
  protected function resolveValues($value, array $args, ResolveContext $context, ResolveInfo $info) {
    if ($value instanceof ImageItem && $value->entity && $value->entity->access('view')) {
      /** @var \Drupal\rokka\Entity\RokkaMetadata $rokka_metadata */
      $rokka_metadata = $value->entity->rokka_metadata;
      if ($rokka_metadata && !empty($rokka_metadata->format)) {
        yield $rokka_metadata->filename;
      }
    }
  }

}
